<?php
$inputs = filter_input_array(INPUT_GET, array(
    "username" => FILTER_SANITIZE_STRING,
    "player_count" => FILTER_VALIDATE_INT,
    "play_time" => FILTER_VALIDATE_INT,
    "min_age" => FILTER_VALIDATE_INT,
    "uncache" => FILTER_VALIDATE_BOOLEAN
));
extract($inputs);
if ($player_count < 1) $player_count = "";
if ($play_time < 1) $play_time = "";
if ($min_age < 1) $min_age = "";
?>

<style>
    body {
        font-family: Geneva, Arial, Helvetica, sans-serif;
    }
    th {
        font-weight: bold;
    }
</style>

<div>
    Show games from your BoardGameGeek collection that are recommended by other users for the number of people you have.
</div>
<form>
    <input required type="text" name="username" value="<?=$username?>"> : BGG Username<br/>
    <input required type="text" name="player_count" value="<?=$player_count?>"> : Player Count<br/>
    <input type="text" name="play_time" value="<?=$play_time?>"> : Max Play Time<br/>
    <input type="text" name="min_age" value="<?=$min_age?>"> : Minimum Age<br/>
    <input type="checkbox" name="uncache" value="1"> : Reload My BGG Data
    <input type="submit">
</form>

<pre>
<?php


$games_not_found = 0;
$games_found = 0;
$candidates_found = 0;

$cache_hits = 0;
$cache_misses = 0;
$cache_used_stale = 0;
$cache_refreshed_stale = 0;
$xml_fetch = 0;

$mysqli = new mysqli('127.0.0.1', '???', '???', '???');
if ($mysqli->connect_errno) {
    echo "Database connect error: {$mysqli->connect_error}";
    $mysqli->close();
    exit;
}

libxml_use_internal_errors(true);

$ttl = $uncache ? 0 : 86400;
$xml = get_data("collection", $username, $ttl);
if ($xml === FALSE) {
    echo "Could not connect to BGG API.";
    $mysqli->close();
    exit;
}

$collection = @simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA);
$collection = json_decode(json_encode($collection), TRUE);

if (empty($collection["item"])) {
    echo "\nCould not find user: {$username}";
    kill_cache(key_cache("collection", $username));
    exit;
} else {

    foreach ($collection["item"] as $idx => $element) {
        $ary = array(
            "name"    => $element["name"],
            "subtype" => $element["@attributes"]["subtype"],
            "id"      => $element["@attributes"]["objectid"],
            "min"     => $element["stats"]["@attributes"]["minplayers"],
            "max"     => $element["stats"]["@attributes"]["maxplayers"],
            "mintime" => $element["stats"]["@attributes"]["minplaytime"],
            "maxtime" => $element["stats"]["@attributes"]["maxplaytime"],
            "thumbnail" => "https:" . str_replace("_t.", "_mt.", $element["thumbnail"])
        );
        $ary["numplayers"] = ($ary["min"] == $ary["max"] ? $ary["min"] : "{$ary["min"]}-{$ary["max"]}");
        $ary["maxtime"] = ($ary["maxtime"] > 0 ? $ary["maxtime"] : $ary["mintime"]);
        $ary["time"] = ($ary["mintime"] == $ary["maxtime"] ? $ary["mintime"] : "{$ary["mintime"]}-{$ary["maxtime"]}");
        $games[] = $ary;
    }

    if (empty($games)) {
        echo "Nothing found in collection.";
    } else {

        foreach ($games as $idx => $game) {

            ++$games_found;
            if ($game["max"] >= $player_count && $game["min"] <= $player_count &&
                ($play_time == 0 || ($play_time > 0 && $game["mintime"] <= $play_time))) {

                ++$candidates_found;
                $xml = get_data("game", $game["id"], 86400*7);
                if ($xml === FALSE) {
                    ++$games_not_found;
                    $games[$idx]["answer"][0] = "GAME {$game["id"]} NOT FOUND";
                } else {
                    $game_info = @simplexml_load_string($xml, "SimpleXMLElement", LIBXML_NOCDATA);
                    $game_info = json_decode(json_encode($game_info), TRUE);

                    $games[$idx]["bgglink"] = "https://boardgamegeek.com/boardgame/" . $game["id"];
                    $games[$idx]["minage"] = $game_info["item"]["minage"]["@attributes"]["value"];

                    if ($min_age > 0 && $min_age < $games[$idx]["minage"]) {
                        --$candidates_found;
                        unset($games[$idx]);
                    }
                    else if (empty($game_info["item"]["poll"])) {
                        $games[$idx]["answer"][0] = "NO POLL";
                    } else {
                        foreach ($game_info["item"]["poll"] as $idx2 => $poll) {
                            if ($poll["@attributes"]["name"] == "suggested_numplayers") {
                                if (empty($poll["results"])) {
                                    $games[$idx]["answer"][0] = "NO RESULTS";
                                } else {
                                    foreach ($poll["results"] as $idx3 => $result) {
                                        $numplayers = $result["@attributes"]["numplayers"];
                                        $max = 0;
                                        $games[$idx]["answer"][$numplayers] = "";
                                        if (empty($result["result"])) {
                                            $games[$idx]["answer"][0] = "NO ANSWERS";
                                        } else {
                                            foreach ($result["result"] as $idx4 => $answer) {
                                                $numvotes = $answer["@attributes"]["numvotes"];
                                                if ($numvotes >= $max) {
                                                    $max = $numvotes;
                                                    $games[$idx]["answer"][$numplayers] = $answer["@attributes"]["value"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                unset($games[$idx]);
            }
        }
    }

    echo "Found {$games_found} games in the collection for {$username}.\n";
    echo "{$candidates_found} games support the player count of {$player_count}.\n";
    echo "Could not find BGG info for {$games_not_found} of those.\n";
    echo "\n";
    echo "cache hits: {$cache_hits}\n";
    echo "cache used stale: {$cache_used_stale}\n";
    echo "cache refreshed stale: {$cache_refreshed_stale}\n";
    echo "cache misses: {$cache_misses}\n";
    echo "</pre>";

    display($games, $player_count);

}


function get_data($thing, $id, $max_age_in_seconds=604800) {

    global $cache_hits;
    global $cache_misses;
    global $cache_used_stale;
    global $cache_refreshed_stale;

    $ret = FALSE;

    $cache = get_cache(key_cache($thing, $id), $max_age_in_seconds);
    if ($cache === FALSE || !empty($cache["stale"])) {
        // cache miss or is stale
        if ($cache === FALSE) ++$cache_misses;
        if ($thing == "game") {
            $url = "https://www.boardgamegeek.com/xmlapi2/thing?id={$id}";
        } else {
            $url = "https://www.boardgamegeek.com/xmlapi2/collection?username={$id}&own=1";
        }
        $ret = @file_get_contents($url);
        if ($ret === FALSE) {
            // API fail, use stale data
            ++$cache_used_stale;
            $ret = $cache["data"];
        } else {
            // API success, add to or update cache
            ++$cache_refreshed_stale;
            put_cache(key_cache($thing, $id), $ret);
        }
    } else {
        // cache hit
        ++$cache_hits;
        $ret = $cache["data"];
    }

    return $ret;
}


// default TTL of 7 days
function get_cache($key, $max_age_in_seconds=604800) {

    global $mysqli;

    $ret = FALSE;

    $sql = "SELECT * FROM bgg_cache WHERE bgg_cache_key = '{$key}' LIMIT 1";
    if (($result = $mysqli->query($sql)) && ($result->num_rows === 1)) {
        $data = $result->fetch_assoc();
        $stale = (time() - strtotime($data["created"])) > $max_age_in_seconds;
        $ret = array(
            "stale" => $stale,
            "key" => $key,
            "created" => $data["created"],
            "data" => $data["value"]
        );
        $result->free();
    }
    return $ret;
}


function put_cache($key, $value) {
    global $mysqli;
    $key = $mysqli->real_escape_string($key);
    $value = $mysqli->real_escape_string($value);
    $created = date("Y-m-d H:i:s");
    $sql = "INSERT INTO `bgg_cache`
            (`bgg_cache_key`, `created`, `value`) VALUES ('{$key}', '{$created}', '{$value}')
            ON DUPLICATE KEY UPDATE `created`='{$created}', `value`='{$value}'";
    $mysqli->query($sql);
}


function kill_cache($key) {
    global $mysqli;
    $key = $mysqli->real_escape_string($key);
    $sql = "DELETE FROM `bgg_cache` WHERE `bgg_cache_key` = '{$key}'";
    $mysqli->query($sql);
}

function key_cache($thing, $id) {
    return $thing . $id;
}


function display ($games, $num_players) {

    $nodata = 0;
    $bgcolor = array(
        "Best" => "green",
        "Recommended" => "yellow",
        "Not Recommended" => "red"
    );

    echo "<table border=1 cellpadding=5 cellspacing=0>";

    foreach (array("Best", "Recommended", "Not Recommended") as $level) {
        ?> <tr><td colspan=6 bgcolor="<?=$bgcolor[$level]?>"><h2><?=$level?></h2></td></tr>
        <tr>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Players</th>
            <th>Time</th>
            <th>Min Age</th>
            <th>Recommendation</th>
        </tr>
        <?php
        foreach ($games as $idx => $game) {
            if ($game["answer"][$num_players] == $level) { ?>
                <tr>
                    <td><a href="<?=$game["bgglink"]?>"><img src="<?=$game["thumbnail"]?>"></a></td>
                    <td><a href="<?=$game["bgglink"]?>"><?=$game["name"]?></a></td>
                    <td align="center"><?=$game["numplayers"]?></td>
                    <td align="center"><?=$game["time"]?> mins</td>
                    <td align="center"><?=$game["minage"]?> yo</td>
                    <td bgcolor="<?=$bgcolor[$level]?>" align="center"><?=$level?></td>
                </tr>
                <?php
            }
            if (!empty($game["answer"][0])) ++$nopoll;
        }
    }

    if ($nopoll) {
        echo "<tr><td colspan=6 bgcolor=silver><h2>UNKNOWN</h2></td></tr>";
        foreach ($games as $idx => $game) {
            if (!empty($game["answer"][0])) { ?>
                <tr>
                    <td><a href="<?=$game["bgglink"]?>"><img src="<?=$game["thumbnail"]?>"></a></td>
                    <td><a href="<?=$game["bgglink"]?>"><?=$game["name"]?></a></td>
                    <td><?=$game["numplayers"]?></td>
                    <td><?=$game["time"]?></td>
                    <td><?=$game["minage"]?></td>
                    <td bgcolor="silver"><?=$game["answer"][0]?></td>
                </tr>
            <?php
            }
        }
    }

    echo "</table>";
}
